package com.example.intentservice

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnStart: Button = findViewById(R.id.btnStart)
        val btnStop: Button = findViewById(R.id.btnStop)
        val tvService: TextView = findViewById(R.id.tvServiceinfo)
        val edtData: EditText = findViewById(R.id.edtData)
        val btnSendData: Button = findViewById(R.id.btnSendData)

        btnStart.setOnClickListener {
            Intent(this, MyService::class.java).also {
                startService(it)
                tvService.text = "Service running"
            }
        }

        btnStop.setOnClickListener {
            Intent(this, MyService::class.java).also {
                stopService(it)
                tvService.text = "Service stopped"
            }
        }

        btnSendData.setOnClickListener {
            val dataString = edtData.text.toString()
            Intent(this, MyService::class.java).also {
                it.putExtra("EXTRA_DATA", dataString)
                startService(it)
            }
        }
    }
}